from .buzzwords import BuzzwordGenerator
from .tex_generators import FrameGenerator, MainGenerator
from .utils import cd
import os
import subprocess
import sys


class BeamerGenerator:
    """Random buzzword beamer generator. Creates a beamer powerpoint with a
    given number of pages with random title taken from a buzzword list."""

    def __init__(self, workdir, n_frames=10):
        """Beamer generator init method.

        Parameters
        ----------
        workdir : str
                  The directory where the files will be written.
        n_frames : int, optional
                   The number of frames for the beamer presentation.
        """
        self.n_frames = n_frames
        self.workdir = os.path.abspath(workdir)
        if not os.path.exists(self.workdir):
            os.mkdir(self.workdir)
        self.main_name = self.generate_presentation()
        self.compile_presentation()
        self.show_presentation()

    def show_presentation(self):
        """Show the presentation (need to be compiled first).
        """
        pdf = os.path.basename(self.main_name.replace(".tex", ".pdf"))
        # detect viewer
        if sys.platform == "linux":
            viewer = "xdg-open"
        elif sys.platform == "darwin":
            viewer = "open"
        else:
            raise NotImplementedError(f"{sys.platform} not supported...")
        with cd(self.workdir):
            subprocess.run([viewer, pdf])

    def compile_presentation(self):
        """Compile presentation (files must be written first).
        """
        with cd(self.workdir):
            subprocess.run(["lualatex", self.main_name])

    def generate_presentation(self):
        """Generates the presentation."""
        frames = self.create_frames(self.n_frames)
        return self.create_main(frames)

    def create_main(self, frames):
        """Generates the main TeX file."""
        main = MainGenerator(self.workdir, frames)
        return main.filename

    def create_frames(self, n):
        """Generates the frames in the presentation."""
        frames = []
        allbuzz = []
        for i in range(n):
            buzz = BuzzwordGenerator()
            # check that buzz word does not already exist in the beamer
            while buzz.buzzword in allbuzz:
                buzz = BuzzwordGenerator()
            allbuzz.append(buzz)
            frame = FrameGenerator(buzz, self.workdir)
            frames.append(frame)
        return frames
