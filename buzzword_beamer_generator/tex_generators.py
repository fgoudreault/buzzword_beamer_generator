from .content_generator import GraphGenerator, TableGenerator
from random import randint
import os


POSSIBLE_CONTENT = ["graph", "table"]


class FrameGenerator:
    """Generates the TeX file for a frame."""

    def __init__(self, buzzword, workdir):
        """Frame generator init method.

        Parameters
        ----------
        buzzword : BuzzwordGenerator instance
                   The buzzword for the frame. It will be the frame title and
                   filename.
        workdir : str
                  The directory path where the frame will be written.
        """
        self.workdir = os.path.abspath(workdir)
        self._buzzword = buzzword
        self.title = str(self._buzzword)
        self.filename = self.title.replace(" ", "_") + ".tex"
        self.filepath = os.path.join(self.workdir, self.filename)
        self.content = POSSIBLE_CONTENT[randint(0, len(POSSIBLE_CONTENT) - 1)]
        self.generate_frame()

    def generate_frame(self):
        """Generates the frame file.
        """
        lines = self.create_lines()
        with open(self.filepath, "w") as f:
            for line in lines:
                if not line.endswith("\n"):
                    line += "\n"
                f.write(line)

    def create_lines(self):
        """Returns the lines that will be written in the frame file.
        """
        lines = []
        # need 2 \\ to escape the \b and \f
        lines.append("\\begin{frame}")
        lines.append("\\frametitle{%s}" % self.title)
        if self.content == "graph":
            content = GraphGenerator(self.workdir, self._buzzword)
        elif self.content == "table":
            content = TableGenerator()
        lines += content.code
        lines.append("\end{frame}")
        return lines


class MainGenerator:
    """Main TeX file generator."""

    def __init__(self, workdir, frames, filename="main"):
        """Main generator init method.

        Parameters
        ----------
        workdir : str
                  The directory where the file will be written.
        frames : list
                 List of frames which will be in the beamer. Must be the paths
                 or directly the FrameGenerator objects. The frames must be
                 writen in the same working directory.
        filename : str, optional
                   The file name.
        """
        if not filename.endswith(".tex"):
            filename += ".tex"
        self.filename = filename
        self.workdir = os.path.abspath(workdir)
        self.filepath = os.path.join(self.workdir, self.filename)
        self.frames = frames
        self.generate_main()

    def generate_main(self):
        """Generate main TeX file."""
        lines = self.create_lines()
        with open(self.filepath, "w") as f:
            for line in lines:
                if not line.endswith("\n"):
                    line += "\n"
                f.write(line)

    def create_lines(self):
        lines = []
        # use 2 \\ to escape \b
        lines.append("\documentclass{beamer}")
        lines.append("\\begin{document}")
        for frame in self.frames:
            if isinstance(frame, FrameGenerator):
                # assume file is in same working directory.
                if self.workdir != frame.workdir:
                    raise FileNotFoundError(f"Frame not in same working"
                                            f" directory: {frame}")
                frame = frame.filename
            if frame.endswith(".tex"):
                frame.strip(".tex")
            # check that frame exists
            if not os.path.isfile(os.path.join(self.workdir, frame)):
                raise FileNotFoundError(f"Frame file does not exists:"
                                        f" {frame}.")
            if " " in frame:
                raise SyntaxError(f"There is spaces in the frame file name:"
                                  f" {frame}.")
            lines.append("\input{%s}" % frame)
        lines.append("\end{document}")
        return lines
