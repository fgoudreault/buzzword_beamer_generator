from random import randint
import matplotlib.pyplot as plt
import numpy as np
import os


class TableGenerator:
    """Generates a random table."""

    def __init__(self):
        """Table generator init method."""
        # only need to generate code
        self.code = self.generate_table()

    def generate_table(self):
        n_cols = randint(3, 5)
        n_rows = randint(3, 10)
        alignement = "|" + "c|" * n_cols
        headers = self.generate_table_headers(n_cols)
        lines = []
        lines.append("\\begin{center}")
        lines.append("\\begin{table}")
        lines.append("\\begin{tabular}{%s}" % alignement)
        # add headers
        lines.append("\hline")
        lines.append(" & ".join(headers) + "\\\\")
        lines.append("\hline")
        # add data
        for i in range(n_rows):
            data = np.random.rand(n_cols)
            data = [str(round(x, 2)) for x in data]
            lines.append(" & ".join(data) + "\\\\")
        lines.append("\hline")
        lines.append("\end{tabular}")
        lines.append("\caption{Dank Table}")
        lines.append("\end{table}")
        lines.append("\end{center}")
        return lines

    def generate_table_headers(self, n_cols):
        # for now just put trivial headers
        return ["data" + str(i + 1) for i in range(n_cols)]


class GraphGenerator:
    """Generates a random graph with a fit."""

    def __init__(self, workdir, buzzword):
        """Graph generator init method.

        Parameters
        ----------
        workdir : str
                  Where the graph will be saved.
        buzzword : BuzzwordGenerator instance.
        """
        self.workdir = os.path.abspath(workdir)
        self.buzzword = buzzword
        self.figname = self.generate_graph()
        self.code = self.generate_code()

    def generate_code(self):
        code = []
        # use 2 \ to escape the \b \f sequences
        code.append("\\begin{figure}")
        code.append("\\includegraphics[width=0.7\linewidth]{%s}" %
                    self.figname)
        code.append("\caption{Clearly a gaussian.}")
        code.append("\end{figure}")
        return code

    def generate_graph(self):
        # random number of points
        npts = randint(10, 50)
        # generate data
        xdata = np.random.rand(npts)
        ydata = np.random.rand(npts)

        # generate plot
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.scatter(xdata, ydata, alpha=0.5)
        ax.set_xlabel("x")
        ax.set_ylabel("y")

        # save figure
        figname = self.buzzword.base.replace(" ", "_") + ".png"
        path = os.path.join(self.workdir, figname)
        plt.savefig(path)
        return figname
