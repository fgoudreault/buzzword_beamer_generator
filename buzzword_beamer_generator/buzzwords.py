from random import randint, sample, shuffle


BASES = ["machine learning",
         "topologic insulator",
         "room temperature superconductor",
         "weyl semi metals",
         "majorana fermions",
         "mott insulator",
         "anyon braider",
         "topological qubits",
         "cluster DMFT",
         "odd-frequency superconductor",
         "topological solar energy",
         "holographic wind power",
         "global warming",
         "non-local locality",
         "deep learning",
         "block chain",
         "non-linear locality",
         "Elon Musk",
         "chiral causality",
         "heterostructure",
         "out-of-equilibrium topology",
         "skirmions",
         "machine learning induced superconductivity",
         "hall effect",
         "graphene",
         "dark matter",
         "dark energy",
         "QED",
         "Higgs boson",
         "linux",
         "Trump effect",
         ]


EXTENSIONS = ["quantum", "fractional", "entangled", "endspace", "fluctional",
              "emergent", "frustrated", "superconducting"]


class BuzzwordGenerator:
    """Generates a buzz word."""

    def __init__(self, max_ext=len(EXTENSIONS)):
        """Random buzzword generator.
    
        Parameters
        ----------
        max_ent : int, optional
                  Maximum number of extensions.
        """
        if max_ext > len(EXTENSIONS):
            raise ValueError(f"Cannot have more extensions than number"
                             f" of extensions available ({len(EXTENSIONS)}).")
        # choose a random base
        self.base = BASES[randint(0, len(BASES) - 1)]
        self.buzzword = self.generate_buzzword(self.base, max_ext)

    def generate_buzzword(self, base, max_ext):
        # choose a random number of extensions
        # shuffle ext list first
        extscopy = EXTENSIONS.copy()
        shuffle(extscopy)
        exts = sample(extscopy, max_ext)
        # add base to list than return result
        exts.append(base)
        return " ".join(exts)

    def __str__(self):
        return self.buzzword

    def __repr__(self):
        return str(self)
