import os


class cd:
    """Context manager for changing the current working directory.
    Taken from https://stackoverflow.com/a/13197763/6362595
    """
    def __init__(self, newPath):
        self.newPath = os.path.abspath(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)
