from setuptools import setup


with open("requirements.txt") as f:
    requirements = f.read().splitlines()


setup(name="buzzword_beamer_generator",
      description=("Python package to create random beamer presentation"
                   " consisting of buzzwords."),
      python_requires=">=3.6",
      install_requires=requirements)
