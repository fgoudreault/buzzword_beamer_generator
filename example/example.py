from buzzword_beamer_generator import BeamerGenerator


# path where the presentation will be generated
path = "./test"
# number of frames
n_frames = 10

BeamerGenerator(path, n_frames=n_frames)
