Welcome to Random Buzzword Beamer Generator
===========================================

This is version 0.2.0 for the moment.
This python package creates random beamer presentation which
titles are random buzzword. 

Requirements
------------

This project requires a LaTeX compiler preinstalled. It must be directly
accessible (exported in the PATH variable at least).

Installation
------------

Clone the project::

  git clone git@gitlab.com:fgoudreault/buzzword_beamer_generator.git
  cd buzzword_beamer_generator

Install python requirements and the project::

  pip install -r requirements.txt
  pip install -e .

Usage
-----

There is an example script in the example directory.
MWE::
  
  from buzzword_beamer_generator import BeamerGenerator

  path = "directory/where/presentation/will/be/written"
  n_frames = 100

  BeamerGenerator(path, n_frames=n_frames)

Future Projects
---------------

- Title page
- Table of content
- intro/conclu
- Acknowledgment
- Bibliography

Future code projects
--------------------

- unittests

Acknowledgements
----------------

- `fgoudreault <https://gitlab.com/fgoudreault>`__

References
----------

[1] https://arxiv.org/abs/1808.02527
