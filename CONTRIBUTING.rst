Contributing Guide
==================

Just submit your merge requests. You can also create issues to ask for features
or flag bugs/errors.